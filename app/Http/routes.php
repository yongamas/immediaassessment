<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
use App\Image;



Route::group(['middleware' => ['web']], function () {
    // Authentication routes...
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Auth\AuthController@getLogout');

    // Registration routes...
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');     
    /**
     * Show Dashboard
     */ 
    Route::get('/', function(){
        if (Auth::check()){
            $ip = getenv('HTTP_CLIENT_IP');

            if (!is_string($ip) || strlen($ip) < 1 || $ip == '127.0.0.1' || $ip == 'localhost')
                $ip = '196.2.164.250';
            
            $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

            return view('images', [
                'images' => Image::where('user_id','=', Auth::user()->id)->orderBy('created_at', 'asc')->get(),
                'location' =>  $details,
                'user_id' => Auth::user()->id
            ]);
        }else{
            return redirect('auth/login');
        }
        
    });

    /**
     * User defined Location  search
     */
    Route::post('/search','ImmediaController@processImages');
});
