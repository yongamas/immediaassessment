<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Jcroll\FoursquareApiClient\Client\FoursquareClient;


define('FOURSQAURE_CLIENT_ID','AGRNU2EKUJNALYF3FJKQEGZLMXDRQCNAHDGZEPHV5QMUPEQ2');
define('FOURSQAURE_CLIENT_SECRET','NCGG33YZ5ESQRR22V0X0AUQEH2UKK54EJ3EQMEIUDLZ1H03V');
define('INSTAGRAM_CLIENT_ID','d681de5f5d1b45fe82a6e178be9778cf');
define('INSTAGRAM_CLIENT_SECRET','60466e7c181a4dc1930a2db0e52697c7');
define('INSTAGRAM_ACCESS_TOKEN','1007728498.2460487.b3a4b4712fc84250ab6e7e1108cb1c83');

class ImmediaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function processImages(Request $request){
        
        if(isset($_POST['current-location'])){
            $codorinates = explode(',', $_POST['current-location']);
                        // Search instagram for images around this location
            $json = file_get_contents("https://api.instagram.com/v1/media/search?lat=". $codorinates[0] . "&lng=" . $codorinates[1]."&access_token=2912979.87fdd31.0949d22f4a714349ae84643c5af165ef");
            $data = json_decode($json);
            
            // Save each image with the location name as it's title
            foreach( $data->data as $user_data ) {
                $image = new Image();
                $image->title = $user_data->location->name;
                $image->thumbnail = $user_data->images->thumbnail->url;
                $image->lowres = $user_data->images->low_resolution->url;
                $image->highres = $user_data->images->standard_resolution->url;
                $image->user_id = $request->user_id;
                if(!$this->checkDuplicate($user_data->images->thumbnail->url)){
                    continue;
                }
                
                $image->save();
            }
        }
        
        if(isset($_POST['db_search'])){
            $image_title = $request->name;
            return view('images', [
                'images' => DB::table('images')->where('title', 'LIKE', '%'.$image_title.'%')->get(),
                'location' => $this->getLocation()
            ]);
        }
        
        // Validate user suppllied location
        $this->validate($request, [
            'name' => 'required|min:4|max:255',
        ]);
        
        // Prepare the foursqure library to get the geo information of the location
        $client = FoursquareClient::factory(array(
            'client_id'     => FOURSQAURE_CLIENT_ID,    
            'client_secret' => FOURSQAURE_CLIENT_SECRET
        ));

        $command = $client->getCommand('venues/search', array(
            'near' => $request->name,
        ));
        
        try{            
             $results = $command->execute();
             if(!$results){
                 throw new Exception('No Results');
             }
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
        
        $locations = $results['response']['venues'];
        
        foreach($locations as $location){
            
            // Search instagram for images around this location
            $json = file_get_contents("https://api.instagram.com/v1/media/search?lat=". $location['location']['lat'] . "&lng=" . $location['location']['lng']."&access_token=2912979.87fdd31.0949d22f4a714349ae84643c5af165ef");
            $data = json_decode($json);
            
            // Save each image with the location name as it's title
            foreach( $data->data as $user_data ) {
                $image = new Image();
                $image->title = $user_data->location->name;
                $image->thumbnail = $user_data->images->thumbnail->url;
                $image->lowres = $user_data->images->low_resolution->url;
                $image->highres = $user_data->images->standard_resolution->url;
                $image->user_id = $request->user_id;
                
                if(!$this->checkDuplicate($user_data->images->thumbnail->url)){
                    continue;
                }
                
                $image->save();
            }

            
            return redirect('/');          
        }
    }
    
    public function execCurl($url = ''){
        $ch = curl_init();
        curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => 2
        ));

        $result = curl_exec($ch);
        curl_close($ch); 
        
        return $result;
    }
    
    public function checkDuplicate($img_url = ''){
        
        $new_filename = $this->getFilenameFromUrl($img_url);
        $images = new Image();
        $thumbnails = $images::select('thumbnail')->get();
        
        foreach ($thumbnails as $thumbnail)
        {
            $saved_filename = $this->getFilenameFromUrl($thumbnail->thumbnail);
            
            if($saved_filename == $new_filename){
                return false;
            }
        }  
        return true;
    }
    
    public function getFilenameFromUrl($url){
        $url_segments = explode('/', $url);
        $last_segment = explode('?', end($url_segments));
        return $last_segment[0];
    }
    public function getLocation(){
        $ip = getenv('HTTP_CLIENT_IP');

        if (!is_string($ip) || strlen($ip) < 1 || $ip == '127.0.0.1' || $ip == 'localhost')
            $ip = '196.2.164.250';

        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
        return $details;
    }
    
}