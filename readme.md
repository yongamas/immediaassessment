For the database, a migration has to be run.
 > php artisan migrate

Uses https://github.com/jcroll/foursquare-api-client Library

Setting up.

1. Clone the Repo https://yongamas@bitbucket.org/yongamas/immediaassessment.git
2. Edit the .env file with your db connection details
3. In the root folder of the application run: composer update or composer install.
4. To create the database table, in the root folder of the application run: php artisan migrate

How it works

One the landing page of the application the user is provided with a form were they can search for images on instagram by location. Once they click search, the results are then displayed at the bottom of the form. The images listing are smaller low res images which when clicked pop up in a lightbox. The images search results are also stored, so when the user lands on the application again, they will see an image listing of their previous searches.

Hours Spent: 9