@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Hi {{ Auth::user()->name }}, Search Instagram Images
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <form action="/search" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Location</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="task-name" class="form-control" value="{{ old('task') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Use Current Location</label>

                            <div class="col-sm-6">
                                <input type="checkbox" name="current-location" id="current-location" value="{{ $location->loc }}"> {{ $location->city }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Database Search</label>

                            <div class="col-sm-6">
                                <input type="checkbox" name="db_search" id="db-search"> 
                            </div>
                        </div>
                        <input type="hidden" name="user_id" id="user_id" value="{{ $user_id }}"> 
                        <div>
                            <!--<div id="geo" class="geolocation_data"></div>-->
                            <!--<p>{{ var_dump($location) }}</p>-->
                        </div>
                        <!-- Add Task Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-search"></i>Search
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Current Tasks -->
            @if (count($images) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Photos From Instagram
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped task-table" id="images-table">
                            <thead>
                                <th>Results</th>
                            </thead>
                            <tbody>
                                @foreach ($images as $image)
                                    <tr>
                                        <td class="table-text">
                                            <div class="image-holder">
                                                <h4>{{ $image->title }}<h4>
                                                <a href="{{ $image->highres }}" class="fancybox-img" title="{{ $image->title }}"><img src="{{ $image->lowres }}" class="image"></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
            
        </div>
    </div>
@endsection
