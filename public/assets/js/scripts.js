$(document).ready(function() {
    $('.fancybox-img').fancybox();
    $('#images-table').DataTable({
        "aLengthMenu": [[5, 10,25,50,100, -1], [5, 10,25,50,100,"All"]],
        "iDisplayLength": 5                
    });
    $('#current-location').click(function(){
        if($('#db-search').is(":checked")){
            $('#db-search').attr('checked', false);
        }
    });
    $('#db-search').click(function(){
        if($('#current-location').is(":checked")){
            $('#current-location').attr('checked', false);
        }
    });
});
